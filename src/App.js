
import React from "react";
import './App.css';
import User from './components/User'

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      users: [],
      posts: [],
      comments: [],
      status: false,

    };

  };

  componentDidMount() {
    fetch("https://jsonplaceholder.typicode.com/users").then((res) => {
      return res.json();
    }).then((data) => {

      this.setState({
        users: data,

      })
      return fetch("https://jsonplaceholder.typicode.com/posts");
    }).then((res) => {
      return res.json();
    }).then((data) => {

      this.setState({
        posts: data,
      })
      return fetch("https://jsonplaceholder.typicode.com/comments");
    }).then((res) => {
      return res.json();
    }).then((data) => {

      this.setState({
        comments: data,
        status: true

      })

    });

  };
  render() {
    return (
      <div className="App">
        <User users={this.state.users} posts={this.state.posts} comments={this.state.comments} />
      </div>

    );
  };
};
export default App;





