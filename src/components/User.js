import React from 'react';
import Post from './Post';

class User extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (<>

            <div>
                <h1 className='heading'>USER'S IMFORMATION</h1>
                {
                    this.props.users.map((user) => {

                        return (<div key={user.id} className='user-container'>
                            <img className='image' src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQZlhonvIAW1NZs7kJYW1nAftLzJGa1m-zkHdz10ob4O-Pa5n9GnMCQJRmOeQXVS2aYraM&usqp=CAU" />
                            <div className='header'>
                                <div className='bold-black'>   <span className='bold'> Name </span> : {user.name} </div>
                                <div className='bold-black'> <span className='bold'> Username </span> : {user.username}</div>
                            </div>
                            <div className='address bold'>  Address :  </div>
                            <div className='addressField'>
                                <span>{user.address.street} {user.address.suite}, {user.address.city}-{user.address.zipcode}</span>
                            </div>
                            <div className='contact'>
                                <span className='bold'>Contact </span>: {user.phone}
                            </div>
                            <div className='contact'>
                                <span className='bold'>website </span>: {user.website}
                            </div>

                            <Post id={user.id} posts={this.props.posts} comments={this.props.comments} />

                        </div>)
                    })
                };
            </div> </>)
    };


};

export default User;