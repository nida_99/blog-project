import React from "react";

class Comments extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            newArr: [],
            showData: false,

        };
    }

    handleClick = (id) => {

        const newArr = this.props.comments.filter((ele) => ele.postId === id)
        this.setState({
            newArr: newArr,
            showData: !this.state.showData,
        })

    }

    render() {
        return (
            <>
                <button onClick={() => this.handleClick(this.props.id)} className="bold button">Comments</button>
                <div className="comment-container">
                    {

                        this.state.showData ?


                            this.state.newArr.map((ele) => {

                                if (this.props.id === ele.postId) {
                                    return (<div key={ele.id} className='comment'>
                                        {
                                            <div>
                                                <div className="header">
                                                    <h4>{ele.name}</h4>
                                                    <p>{ele.email}</p>
                                                </div>
                                                <p className="para">{ele.body}</p>
                                            </div>
                                        }

                                    </div>)

                                };

                            })

                            :

                            undefined


                    }
                </div>
            </>
        );
    };
};

export default Comments