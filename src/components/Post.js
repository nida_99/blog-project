import React from "react";
import Comments from "./Comments";
import '../App.css'


class Post extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            newArr: [],
            showData: false,

        };
    }

    handleClick = (id) => {
        const newArr = this.props.posts.filter((ele) => ele.userId === id)
        this.setState({
            newArr: newArr,
            showData: !this.state.showData,
        })
        return newArr;
    }

    render() {
        return (
            <>
                <button onClick={() => this.handleClick(this.props.id)} className="bold button">Posts</button>
                <div className="post-container">
                    {
                        this.state.showData ?
                            this.state.newArr.map((ele) => {

                                if (this.props.id === ele.userId) {
                                    return (
                                        <div key={ele.id} className='posts'>

                                            <div className="title">{ele.title}</div>
                                            <p className="para">{ele.body}</p>
                                            <Comments id={ele.id} comments={this.props.comments} />
                                        </div>
                                    );
                                };

                            })

                            :

                            undefined
                    }
                </div>
            </>
        );
    };
};

export default Post